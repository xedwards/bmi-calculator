﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BMI_Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            // Initalize our Variables
            string name, weight, str_feet, str_inches;
            int inches;
            double feet, pounds, BMI;

            //Set the Title of Our Program
            Console.Title = "BMI Calculator";
            
            //Welcome the User
            Console.WriteLine("\tWelcome to the " + Console.Title + " Program.");
            Console.Write("I'm goint to ask you your height and weight " +
                "and tell you your Body Mass Index");

            //Get Users Name
            Console.Write("\nBefore we begin, what is your name, small pleb?");
            name = Console.ReadLine();
            Console.WriteLine("\nNice to meet you " + name + ".");

            Console.Write("\nPress enter to quit");
            Console.Read();
        }
    }
}
